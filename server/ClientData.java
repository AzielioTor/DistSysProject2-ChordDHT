/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import client.Client;

/**
 * This class handles the connection of the clients connected to this network.
 * @author AzielioTor
 */
public class ClientData {

    private Client client;
    private int clientNum;
    private boolean online;
    private String IP_Address;

    public ClientData(Client client, int clientNum, boolean online, String IP) {
        this.client = client;
        this.clientNum = clientNum;
        this.online = online;
        this.IP_Address = IP;
    }

    public ClientData(int clientNum, boolean online, String IP) {
        this(null, clientNum, online, IP);
    }

    public void turnClientOffline() {
        this.IP_Address = null;
        this.online = false;
    }

    public void turnClientOnline(String IP) {
        this.IP_Address = IP;
        this.online = true;
    }

    public void updateClientIP(String IP) {
        this.IP_Address = IP;
    }

    public String getClientIP() {
        return this.IP_Address;
    }

    public Client getClient() {
        return this.client;
    }

    public int getClientNum() {
        return this.clientNum;
    }

    public boolean getClientConnectionStatus() {
        return this.online;
    }

    public void updateClientData(int clientNum, boolean online, String IP) {
        this.clientNum = clientNum;
        this.online = online;
        this.IP_Address = IP;
    }

    @Override
    public String toString() {
        return "ClientNode : " + getClientNum() + "   Status : " + online
                + "    IP Address : " + getClientIP();
    }

}
