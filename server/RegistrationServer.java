/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import client.Client;
import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AzielioTor
 */
public class RegistrationServer { //implements ServerNode {

    private int n;
    private int totalNodes;
    private final String IPADDRESS;
    private MessageDigest hash;
    private final static int PORT = 15600;
    private ClientData[] clientData;

    public RegistrationServer(int n) throws UnknownHostException, NoSuchAlgorithmException {
        System.out.println("This Clients IP Address is : " + InetAddress.getLocalHost().getHostAddress());
        this.n = n;
        this.totalNodes = (int) Math.pow(2, n);
        IPADDRESS = InetAddress.getLocalHost().getHostAddress();
        hash = MessageDigest.getInstance("SHA-256");
        clientData = new ClientData[totalNodes];

        initClientData();
    }

    public RegistrationServer() throws UnknownHostException, NoSuchAlgorithmException {
        this(4);
    }

    /**
     * Initialzes Client data
     * @throws UnknownHostException 
     */
    private void initClientData() throws UnknownHostException {
        for (int i = 0; i < this.clientData.length; i++) {
            this.clientData[i] = new ClientData(-1, false, "none");
        }
    }

    public String getIPAddress() {
        return this.IPADDRESS;
    }

    /**
     * Adds client to the registration Server
     * @param c Client to add to the network
     * @return Whether the client was successfully added or not
     */
    public boolean addClient(Client c) {
        int clientNum = c.getClientNodeNum();
        ClientData[] cda = getClientData();
        Client atIndex = cda[clientNum].getClient();
        if (atIndex == null) {
            cda[clientNum] = new ClientData(c, c.getClientNodeNum(), true, c.getClientIP());
            return true;
        }
        return false;
    }

    /**
     * Adds client to the registration server
     * @param nodeNum Node number to add
     * @param status If the node is online or not
     * @param IP IP of the client connection.
     * @return 
     */
    public boolean addClient(int nodeNum, boolean status, String IP) {
        if (getClientData()[nodeNum].getClientIP().equalsIgnoreCase("none")) {
            getClientData()[nodeNum] = new ClientData(null, nodeNum, true, IP);
            return true;
        }
        return false;
    }

    /**
     * Removes the client at this node number
     * @param nodeNum
     * @return 
     */
    public boolean removeClient(int nodeNum) {
        ClientData cd = getClientData()[nodeNum];
        if (cd.getClientConnectionStatus()) {
            cd.updateClientData(-1, false, "none");
            return true;
        } else {
            System.out.println("Client already offline");
            return false;
        }
    }

    public ClientData[] getClientData() {
        return this.clientData;
    }

    /**
     * A handler thread class. Handlers are spawned from the listening loop and
     * are responsible for a dealing with a single client and broadcasting its
     * messages.
     */
    private static class RequestHandler extends Thread {

        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;
        private Dispatcher dispatcher;
        private RegistrationServer rs;

        /**
         * Constructs a handler thread, squirreling away the socket. All the
         * interesting work is done in the run method.
         */
        public RequestHandler(Socket socket, RegistrationServer regServ) {
            this.socket = socket;
            this.rs = regServ;
            // Create a new JSON-RPC 2.0 request dispatcher
            this.dispatcher = new Dispatcher();

            // Register the "echo", "getDate" and "getTime" handlers with it
            dispatcher.register(new JsonHandlerServer.ClientRegistrationHandler(getServer()));

        }

        public RegistrationServer getServer() {
            return this.rs;
        }

        /**
         * Services this thread's client by repeatedly requesting a screen name
         * until a unique one has been submitted, then acknowledges the name and
         * registers the output stream for the client in a global set, then
         * repeatedly gets inputs and broadcasts them.
         */
        public void run() {
            try {
                // Create character streams for the socket.
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

                // read request
                String line;
                line = in.readLine();
                StringBuilder raw = new StringBuilder();
                raw.append("" + line);
                boolean isPost = line.startsWith("POST");
                int contentLength = 0;
                while (!(line = in.readLine()).equals("")) {
                    //System.out.println(line);
                    raw.append('\n' + line);
                    if (isPost) {
                        final String contentHeader = "Content-Length: ";
                        if (line.startsWith(contentHeader)) {
                            contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                        }
                    }
                }
                StringBuilder body = new StringBuilder();
                if (isPost) {
                    int c = 0;
                    for (int i = 0; i < contentLength; i++) {
                        c = in.read();
                        body.append((char) c);
                    }
                }

//                System.out.println(body.toString());
                JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
//                System.out.println("Received Request :  " + request);
                JSONRPC2Response resp = dispatcher.process(request, null);

                // send response
                out.write("HTTP/1.1 200 OK\r\n");
                out.write("Content-Type: application/json\r\n");
                out.write("\r\n");
                out.write(resp.toJSONString());
                // do not in.close();
                out.flush();
                out.close();
            } catch (IOException e) {
                System.out.println(e);
            } catch (JSONRPC2ParseException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    System.err.println(e);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {

        System.out.println("The server is running on PORT :  " + PORT);
        ServerSocket listener = new ServerSocket(PORT);
        Scanner scan = new Scanner(System.in);
        System.out.println("What is the degree of the Chord network...");
        System.out.print(">> ");
        String input = scan.nextLine();
        int degree = Integer.parseInt(input);
        RegistrationServer reserv = new RegistrationServer(degree);
        new PrintNodeStatusThread(reserv).start();
        try {
            while (true) {
                new RequestHandler(listener.accept(), reserv).start();
            }
        } finally {
            listener.close();
        }
    }

    /**
     * This node periodically prints the status of the ndoes connected to the server
     */
    public static class PrintNodeStatusThread extends Thread {

        private RegistrationServer rs;
        private static final int PRINT_SLEEP_TIMER = 15000;

        public PrintNodeStatusThread(RegistrationServer rs) {
            this.rs = rs;
        }

        @Override
        public void run() {
            while (true) {
                ClientData[] cda = rs.getClientData();
                boolean printedOnce = false;
                for (int index = 0; index < cda.length; index++) {
                    if (cda[index].getClientConnectionStatus()) {
                        System.out.println("Node : " + index + "   Status : "
                                + cda[index].getClientConnectionStatus()
                                + "   At IP : " + cda[index].getClientIP());
                        printedOnce = true;
                    }
                }
                if (!printedOnce) {
                    System.out.println("No nodes currently online...");
                }
                System.out.println("");
                try {
                    Thread.sleep(PRINT_SLEEP_TIMER);
                } catch (InterruptedException ex) {
                    System.err.println("SOMETHING WENT WRONG IN PRINTNODESTATUSTHREAD");
                    Logger.getLogger(RegistrationServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
