package server;

/**
 * Demonstration of the JSON-RPC 2.0 Server framework usage. The request
 * handlers are implemented as static nested classes for convenience, but in
 * real life applications may be defined as regular classes within their old
 * source files.
 *
 * @author Vladimir Dzhuvinov
 * @version 2011-03-05
 */
import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;
import java.util.HashMap;
import java.util.Map;

public class JsonHandlerServer {

    // Implements a handler for All Servers
    // that return the current date and time
    public static class ClientRegistrationHandler implements RequestHandler {

        private RegistrationServer rs;

        public ClientRegistrationHandler(RegistrationServer regServ) {
            this.rs = regServ;
        }

        // Reports the method names of the handled requests
        public String[] handledRequests() {

            return new String[]{"printAllNodes", "unregisterClient",
                "registerNode", "getCurrentClientIPs"};
        }

        // Processes the requests
        public JSONRPC2Response process(JSONRPC2Request req, MessageContext ctx) {
            // This statement registers nodes onto the server
            if (req.getMethod().equals("registerNode")) {
                Map hm = req.getNamedParams();
                String clientIP = (String) hm.get("ClientIP");
                int nodeNum = Integer.parseInt((String) hm.get("ClientNodeNum"));
                boolean success = rs.addClient(nodeNum, true, clientIP);
                String responseString = "Client added :   " + success;
                return new JSONRPC2Response(responseString, req.getID());
                // This node returns all nodes currently online on the server
            } else if (req.getMethod().equals("printAllNodes")) {
                String response = "";
                ClientData[] cda = rs.getClientData();
                for (int index = 0; index < cda.length; index++) {
                    if (cda[index].getClientConnectionStatus()) {
                        response += cda[index].toString() + "\n";
                    }
                }
                return new JSONRPC2Response(response, req.getID());
                // This method unregiesters a client from the server
            } else if (req.getMethod().equals("unregisterClient")) {
                int nodeNum = Integer.parseInt((String) req.getNamedParams().get("ClientNodeNum"));
                boolean success = rs.removeClient(nodeNum);
                String response = "Client removal : " + success;
                System.out.println(response);
                return new JSONRPC2Response(response, req.getID());
                // Return list of all online IPs
            } else if (req.getMethod().equals("getCurrentClientIPs")) {
                HashMap<String, Object> response = new HashMap<>();
                Map requestParams = (HashMap) req.getNamedParams();
                ClientData[] cda = rs.getClientData();

                int numParams = (Integer) Integer.parseInt(requestParams.get("numParams") + "");
                String[] storeParams = new String[numParams];
                for (int i = 0; i < numParams; i++) {
                    int k = (Integer) Integer.parseInt(requestParams.get("param" + i) + "");
                    while (cda[k].getClientNum() == -1) {
                        k = (k + 1) % cda.length;
                    }
                    storeParams[i] = (String) requestParams.get("param" + i);
                    response.put("IPparam" + i, cda[k].getClientIP() + "");
                    response.put("CNparam" + i, cda[k].getClientNum() + "");
                }
                int pred = (Integer) Integer.parseInt(requestParams.get("predecessor") + "");
                while (!cda[pred].getClientConnectionStatus()) {
                    pred = pred - 1;
                    if (pred < 0) {
                        pred = pred + cda.length;
                    }
                }
                int actualPred = cda[pred].getClientNum();
                String actualPredIP = cda[pred].getClientIP();
                response.put("actualPred", actualPred + "");
                response.put("actualPredIP", actualPredIP);
                return new JSONRPC2Response(response, req.getID());
            } else {
                return new JSONRPC2Response(JSONRPC2Error.METHOD_NOT_FOUND, req.getID());
            }
        }
    }
}
