/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import com.sun.org.apache.xalan.internal.xsltc.dom.BitArray;
import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;
import fingertable.FingerTable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.math.*;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.JsonHandlerServer;
import server.RegistrationServer;

/**
 *
 * @author AzielioTor
 */
public class Client extends Thread {// implements ClientNode {

    private int nodeNum;
    private String RegistrationServerIP;
    private String thisClientIP;
    private FingerTable fingerTable;
    private int totalNodes;
    private int n;
    private JSONRPC2Response latestResponse;
    private JSONRPC2Response latestSuccessorResponse;
    private static final int PORT = 15600;
    private static final String SERVER_PORT = ":" + PORT;
    private int predecessorNum;
    private int predecessorActual;
    private String predecessorIP;
    private List<String> keys;
    // Key logic between nodes.
    private Socket send;
    private ServerSocket recieve;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;

    /**
     * Constructs the Client object
     * @param nodeNum This Clients Node ID within the ChordDHT network
     * @param n The degree of the network (2^n)
     * @param registrationServerIP The IP of the server which handles registration
     * @throws UnknownHostException Error if host is broken
     * @throws IOException Error with the IO
     */
    public Client(int nodeNum, int n, String registrationServerIP) throws UnknownHostException, IOException {
        this.nodeNum = nodeNum;
        this.n = n;
        this.totalNodes = (int) Math.pow(2, n);
        this.RegistrationServerIP = registrationServerIP;
        this.thisClientIP = InetAddress.getLocalHost().getHostAddress();
        keys = new LinkedList<>();
        System.out.println("This Clients IP Address is : " + getClientIP());
        // Set up FingerTable
        fingerTable = new FingerTable(this, this.nodeNum, this.n);
        // Do predecessor
        this.predecessorNum = this.nodeNum - 1;
        if (this.predecessorNum < 0) {
            this.predecessorNum = this.predecessorNum + this.totalNodes;
        }
        this.predecessorActual = -1;
        this.predecessorIP = "None";
        // Register node on server
        boolean success = registerNodeOnServer();
        String response = (String) this.latestResponse.getResult();
        if (response.contains("false")) {
            System.out.println("Node Already connected. Shutting down...");
            System.exit(1);
        }
        if (success) {
            System.out.println("Connection successful...");
        } else {
            System.err.println("Connection unsuccessful...");
        }
        System.out.println("Starting RequestHandler");
        try {
            recieve = new ServerSocket(PORT);
        } catch (Exception ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public FingerTable getFingerTable() {
        return this.fingerTable;
    }

    /**
     * This method handls all the logic for shutting down the server
     */
    public void closeClient() {
        String method = "unregisterClient";
        int requestID = this.nodeNum;
        HashMap<String, Object> namedParams = new HashMap<>();
        namedParams.put("ClientNodeNum", getClientNodeNum() + "");
        JSONRPC2Request request = new JSONRPC2Request(method, namedParams, requestID);
        boolean succ = sendRequest(request, getServerIP());
        if (succ) {
            System.out.println("Successfully dissconnected from Server.");
            System.out.println(this.latestResponse.getResult());
        } else {
            System.err.println("Unsuccessful dissconnection from Server...");
        }
        if (getFingerTable().getSuccessors()[0] != this.nodeNum) {
            System.out.println("Sending keys...");
            method = "addKey";
            requestID = this.nodeNum;
            namedParams = new HashMap<>();
            for (String k : keys) {
                namedParams.put("key", k);
                request = new JSONRPC2Request(method, namedParams, requestID);
                sendRequest(request, "http://" + getFingerTable().getSuccessorsIPs()[0] + SERVER_PORT);
            }
        }
        return;
    }

    private String getServerIP() {
        return this.RegistrationServerIP;
    }

    public String getClientIP() {
        return this.thisClientIP;
    }

    private void setLatestResponse(JSONRPC2Response r) {
        this.latestResponse = r;
    }

    // Change to private when program is running
    public JSONRPC2Response getLatestResponse() {
        return this.latestResponse;
    }

    private void setLatestSuccessorResponse(JSONRPC2Response r) {
        this.latestSuccessorResponse = r;
    }

    private JSONRPC2Response getLatestSuccessorResponse() {
        return this.latestSuccessorResponse;
    }

    public int getClientNodeNum() {
        return this.nodeNum;
    }

    /**
     * Updates the Clients successors
     * @param successors The list of successors
     * @param ips  The IPs to the successor
     */
    public void updateSuccessors(int[] successors, String[] ips) {
        this.fingerTable.updateSuccessors(successors);
        this.fingerTable.updateSuccessorsIPs(ips);
    }

    /**
     * Registers a node on the Registration server
     * @return Whether the registration was successful
     */
    private boolean registerNodeOnServer() {
        String method = "registerNode";
        int requestID = this.nodeNum;
        HashMap<String, Object> namedParams = new HashMap<>();
        namedParams.put("ClientIP", getClientIP());
        namedParams.put("ClientNodeNum", getClientNodeNum() + "");
        JSONRPC2Request request = new JSONRPC2Request(method, namedParams, requestID);
        return sendRequest(request, getServerIP());
    }

    /**
     * Sends a request and returns a boolean about if it was successful
     * @param request The request to send
     * @param URLstring The URL of the node you want to talk to
     * @return Whether the request was successful or not.
     */
    public boolean sendRequest(JSONRPC2Request request, String URLstring) {
        URL serverURL = null;
        try {
            serverURL = new URL(URLstring);
        } catch (MalformedURLException e) {
            System.err.println("Something went wrong constructing ServerURL");
            System.err.println("Method: sendRequest");
            System.out.println(URLstring);
            System.err.println(e.getMessage());
            return false;
        }

        JSONRPC2Session mySession = new JSONRPC2Session(serverURL);

        JSONRPC2Response response = null;

        try {
            response = mySession.send(request);
        } catch (JSONRPC2SessionException e) {
            System.err.println("Something went wrong sending request/getting response");
            System.err.println("Method: sendServerRequest");
            System.err.println(e.getMessage());
            return false;
        }
        this.setLatestResponse(response);
        if (response.indicatesSuccess()) {
            return true;
        } else {
            System.err.println("Request failed");
            System.err.println(response.getError().getMessage());
            return false;
        }
    }

    /**
     * Sends a request while returning a JSONRPC2REsponse to see what happens
     * @param request The request to send
     * @param URLstring The URL of the Client/Server to send to.
     * @return The response object generated from server/client
     */
    public JSONRPC2Response sendRequestWithResponse(JSONRPC2Request request, String URLstring) {
        URL serverURL = null;
        try {
            serverURL = new URL(URLstring);
        } catch (MalformedURLException e) {
            System.err.println("Something went wrong constructing ServerURL");
            System.err.println("Method: sendRequest");
            System.out.println(URLstring);
            System.err.println(e.getMessage());
            return null;
        }

        JSONRPC2Session mySession = new JSONRPC2Session(serverURL);

        JSONRPC2Response response = null;

        try {
            response = mySession.send(request);
        } catch (JSONRPC2SessionException e) {
            System.err.println("Something went wrong sending request/getting response");
            System.err.println("Method: sendServerRequest");
            System.err.println(e.getMessage());
            return null;
        }
        if (response.indicatesSuccess()) {
            return response;
        } else {
            System.err.println("No Keys found on client");
            return response;
        }
    }

    
    /**
     * Main loop of this class. Takes user input and does methods.
     */
    @Override
    public void run() {
        Scanner scan = new Scanner(System.in);
        while (true) {
            printOptions();
            String input = scan.nextLine();
            switch (input) {
                case "1":
                    System.out.println(getFingerTable());
                    break;
                case "2":
                    printOnlineNodes();
                    break;
                case "3":
                    submitKey(null, -1, -1);
                    break;
                case "4":
                    listHeldKeys();
                    break;
                case "5":
                    removeKey(null, -1, -1);
                    break;
                case "6":
                    findKey(null, -1, false, -1);
                    break;
                case "7":
                    printNodeInfo();
                    break;
                case "8":
                    exit();
                    break;
                default:
                    System.out.println("\nInput not recognized. Please try again.\n");
                    break;
            }
        }
    }

    /**
     * Prints all the relevant info relating to this node.
     */
    public void printNodeInfo() {
        System.out.println("\nYou are node : " + this.getClientNodeNum());
        System.out.println("Your IP address is : " + this.thisClientIP);
        System.out.println("There are " + this.totalNodes + " max nodes allowed in this network.");
        System.out.println("Predecessor node should be : " + this.predecessorNum);
        System.out.println("The actual predecessor node is : " + this.predecessorActual + " at IP : " + this.predecessorIP + "\n");
    }

    /**
     * Inserts a key into the Chord network
     * @param value The key you want to insert
     * @param sourceNode The node where this request originated from
     */
    public void submitKey(String value, int sourceNode, int counter) {
        String input;
        if (value == null) {
            // Get user input for what String is needed.
            System.out.println("Please enter the key value to submit:");
            System.out.print(">> ");
            Scanner scan = new Scanner(System.in);
            input = scan.nextLine();
            System.out.println("");
        } else {
            input = value;
        }
        // Hash key to figure out what node to send it too
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (md == null) {
            System.err.println("Something wrong with Message Digest");
            return;
        }
        md.update(input.getBytes());

        byte[] byteData = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        // Get node information to send too.
        System.out.println("Hex format : " + sb.toString());
        BigInteger hashedBigInt = new BigInteger(byteData);
        System.out.println("Big Int representation (Base 10): " + hashedBigInt);
        Integer i = this.totalNodes;
        byte zero = 0;
        byte[] ba = new byte[]{zero, i.byteValue()};
        BigInteger nodeForKey = hashedBigInt.mod(new BigInteger(ba));
        System.out.println("Key : " + input + " will go to client : " + nodeForKey);
        // If Client node to send is the same one as the current node.
        if (nodeForKey.intValue() == this.nodeNum) {
            System.out.println("This client should store the key value : " + input);
            if (this.keys.contains(input)) {
                System.out.println("This client already has the key : " + input);
            } else {
                System.out.println("This client does not store the key : " + input);
                this.addKeyToList(input);
                System.out.println("Key Stored.\n");
            }
            return;
        }
        // FINGERTABLE VARIABLE SINCE WERE ALWAYS GETTING IT
        FingerTable fingertable = getFingerTable();
        // If client node is a direct successor from Fingertable
        // Get variables needed for checking if 
        int nodeToVisit = -1;
        String nodeToVisitIP = null;
        String[] ips = fingertable.getSuccessorsIPs();
        int[] successors = fingertable.getSuccessors();
        int[] ks = fingertable.getK();
        // Loop to see if node has direct access to the correct node.
        System.out.println("Checking which node to visit...");
        for (int j = 0; j < ips.length; j++) {
            BigInteger jay = new BigInteger(String.valueOf(ks[j]));
            if (jay.equals(nodeForKey)) {
                nodeToVisit = successors[j];
                nodeToVisitIP = ips[j] + SERVER_PORT;
                System.out.println("NODETOVISIT + IP : " + nodeToVisit + " + " + nodeToVisitIP);
            }
        }
        if (nodeToVisitIP != null) {
            String method = "addKey";
            int requestID = this.nodeNum;
            HashMap<String, Object> namedParams = new HashMap<>();
            namedParams.put("key", input);
            JSONRPC2Request request = new JSONRPC2Request(method, namedParams, requestID);
            sendRequest(request, "http://" + nodeToVisitIP);
            return;
        }

        for (int j = 0; j < ips.length; j++) {
            BigInteger jay = new BigInteger(String.valueOf(successors[j]));
            if (jay.equals(nodeForKey)) {
                nodeToVisit = successors[j];
                nodeToVisitIP = ips[j] + SERVER_PORT;
                System.out.println("NODETOVISIT + IP : " + nodeToVisit + " + " + nodeToVisitIP);
            }
        }
        if (nodeToVisitIP != null) {
            HashMap<String, Object> namedParams = new HashMap<>();
            namedParams.put("key", input);
            JSONRPC2Request request = new JSONRPC2Request("addKey", namedParams, this.nodeNum);
            sendRequest(request, "http://" + nodeToVisitIP);
            return;
        }
        // If client node is not on FingerTable - Talk to other nodes.
        // nodeForKey = node the data needs to go to
        // Have access to ks, successors, and SuccessorIPs

        if (counter == -1) {
            counter = 0;
        }
        if (counter == 6) {
            sourceNode = this.nodeNum;
            counter = 0;
        }
        counter++;

        int thisNodeNum = sourceNode;
        if (sourceNode == -1) {
            thisNodeNum = this.nodeNum;
        }
        if (this.nodeNum == sourceNode) {
            this.addKeyToList(input);
            return;
        }

        int targetNode = Integer.parseInt(nodeForKey.toString());
        int[] successorInts = this.fingerTable.getSuccessors();
        String[] successorIPs = this.fingerTable.getSuccessorsIPs();

        for (int j = successorInts.length - 1; j >= 0; j--) {
            int currSucc = successorInts[j];

            if (currSucc < targetNode) {
                HashMap<String, Object> namedParams = new HashMap<>();
                namedParams.put("key", input);
                namedParams.put("sourceNode", thisNodeNum + "");
                namedParams.put("counter", counter + "");
                JSONRPC2Request request = new JSONRPC2Request("forwardKeyAdd", namedParams, this.nodeNum);
                sendRequest(request, "http://" + successorIPs[j - 1] + SERVER_PORT);
                return;
            }
        }
        for (int j = successorInts.length - 1; j >= 0; j--) {
            int currSucc = successorInts[j];
            if (currSucc >= targetNode) {
                this.addKeyToList(input);
            } else if (currSucc > targetNode && j - 1 >= 0 && targetNode > successorInts[j - 1]) {
                HashMap<String, Object> namedParams = new HashMap<>();
                namedParams.put("key", input);
                namedParams.put("sourceNode", thisNodeNum + "");
                namedParams.put("counter", counter + "");
                JSONRPC2Request request = new JSONRPC2Request("forwardKeyAdd", namedParams, this.nodeNum);
                sendRequest(request, "http://" + successorIPs[j] + SERVER_PORT);
                return;
            }
        }
        HashMap<String, Object> namedParams = new HashMap<>();
        namedParams.put("key", input);
        namedParams.put("sourceNode", thisNodeNum + "");
        namedParams.put("counter", counter + "");
        JSONRPC2Request request = new JSONRPC2Request("forwardKeyAdd", namedParams, this.nodeNum);
        sendRequest(request, "http://" + successorIPs[2] + SERVER_PORT);
        return;

    }

    /**
     * Prints out the keys that this node holds
     */
    public void listHeldKeys() {
        System.out.println("");
        if (keys.isEmpty()) {
            System.out.println("This node has no keys.\n");
            return;
        }
        int counter = 0;
        System.out.println("/---------------------------------------\\");
        for (String key : keys) {
            System.out.println("Key" + counter + " >> " + key);
            counter++;
        }
        System.out.println("\\---------------------------------------/");
        System.out.println("");
    }

    /**
     * Removes a specified key from the network
     * @param value The key we want to remove.
     * @param sourceNode The node where this request originiated from
     */
    public void removeKey(String value, int sourceNode, int counter) {
        String input;
        if (value == null) {
            // Get user input for what String is needed.
            System.out.println("Please enter the key value to remove:");
            System.out.print(">> ");
            Scanner scan = new Scanner(System.in);
            input = scan.nextLine();
            System.out.println("");
        } else {
            input = value;
        }
        // Hash key to figure out what node to send it too
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (md == null) {
            System.err.println("Something wrong with Message Digest");
            return;
        }
        md.update(input.getBytes());

        byte[] byteData = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        // Get node information to send too.
        System.out.println("Hex format : " + sb.toString());
        BigInteger hashedBigInt = new BigInteger(byteData);
        System.out.println("Big Int representation (Base 10): " + hashedBigInt);
        Integer i = this.totalNodes;
        byte zero = 0;
        byte[] ba = new byte[]{zero, i.byteValue()};
        BigInteger nodeForKey = hashedBigInt.mod(new BigInteger(ba));
        System.out.println("Key : " + input + " will go to client : " + nodeForKey);
        // If Client node to send is the same one as the current node.
        if (nodeForKey.intValue() == this.nodeNum) {
            System.out.println("This client should store the key value : " + input);
            if (this.keys.contains(input)) {
                System.out.println("This client has the key : " + input);
                System.out.println("Removing...");
                this.removeKeyFromList(input);
            } else {
                System.out.println("This client does not store the key : " + input);
                System.out.println("Key not in network.\n");
            }
            return;
        }
        // FINGERTABLE VARIABLE SINCE WERE ALWAYS GETTING IT
        FingerTable fingertable = getFingerTable();
        // If client node is a direct successor from Fingertable
        // Get variables needed for checking if 
        int nodeToVisit = -1;
        String nodeToVisitIP = null;
        String[] ips = fingertable.getSuccessorsIPs();
        int[] successors = fingertable.getSuccessors();
        int[] ks = fingertable.getK();
        // Loop to see if node has direct access to the correct node.
        System.out.println("Checking which node to visit...");
        for (int j = 0; j < ips.length; j++) {
            BigInteger jay = new BigInteger(String.valueOf(ks[j]));
            if (jay.equals(nodeForKey)) {
                nodeToVisit = successors[j];
                nodeToVisitIP = ips[j] + SERVER_PORT;
                System.out.println("NODETOVISIT + IP : " + nodeToVisit + " + " + nodeToVisitIP);
            }
        }
        if (nodeToVisitIP != null) {
            String method = "removeKey";
            int requestID = this.nodeNum;
            HashMap<String, Object> namedParams = new HashMap<>();
            namedParams.put("key", input);
            JSONRPC2Request request = new JSONRPC2Request(method, namedParams, requestID);
            sendRequest(request, "http://" + nodeToVisitIP);
            return;
        }

        for (int j = 0; j < ips.length; j++) {
            BigInteger jay = new BigInteger(String.valueOf(successors[j]));
            if (jay.equals(nodeForKey)) {
                nodeToVisit = successors[j];
                nodeToVisitIP = ips[j] + SERVER_PORT;
                System.out.println("NODETOVISIT + IP : " + nodeToVisit + " + " + nodeToVisitIP);
            }
        }
        if (nodeToVisitIP != null) {
            HashMap<String, Object> namedParams = new HashMap<>();
            namedParams.put("key", input);
            JSONRPC2Request request = new JSONRPC2Request("removeKey", namedParams, this.nodeNum);
            sendRequest(request, "http://" + nodeToVisitIP);
            return;
        }
        // If client node is not on FingerTable - Talk to other nodes.
        // nodeForKey = node the data needs to go to
        // Have access to ks, successors, and SuccessorIPs

        if (counter == -1) {
            counter = 0;
        }
        if (counter == 6) {
            sourceNode = this.nodeNum;
            counter = 0;
        }
        counter++;

        int thisNodeNum = sourceNode;
        if (sourceNode == -1) {
            thisNodeNum = this.nodeNum;
        }
        if (this.nodeNum == sourceNode) {
            this.removeKeyFromList(input);
            return;
        }

        int targetNode = Integer.parseInt(nodeForKey.toString());
        int[] successorInts = this.fingerTable.getSuccessors();
        String[] successorIPs = this.fingerTable.getSuccessorsIPs();

        for (int j = successorInts.length - 1; j >= 0; j--) {
            int currSucc = successorInts[j];

            if (currSucc < targetNode) {
                HashMap<String, Object> namedParams = new HashMap<>();
                namedParams.put("key", input);
                namedParams.put("sourceNode", thisNodeNum + "");
                namedParams.put("counter", counter + "");
                JSONRPC2Request request = new JSONRPC2Request("forwardKeyRemove", namedParams, this.nodeNum);
                System.out.println(request + "\n" + successorIPs[j - 1]);
                sendRequest(request, "http://" + successorIPs[j - 1] + SERVER_PORT);
                return;
            }
        }
        for (int j = successorInts.length - 1; j >= 0; j--) {
            int currSucc = successorInts[j];
            if (currSucc >= targetNode) {
                this.removeKeyFromList(input);
            } else if (currSucc > targetNode && j - 1 >= 0 && targetNode > successorInts[j - 1]) {
                HashMap<String, Object> namedParams = new HashMap<>();
                namedParams.put("key", input);
                namedParams.put("sourceNode", thisNodeNum + "");
                namedParams.put("counter", counter + "");
                JSONRPC2Request request = new JSONRPC2Request("forwardKeyRemove", namedParams, this.nodeNum);
                System.out.println(request + "\n" + successorIPs[j]);
                sendRequest(request, "http://" + successorIPs[j] + SERVER_PORT);
                return;
            }
        }
        HashMap<String, Object> namedParams = new HashMap<>();
        namedParams.put("key", input);
        namedParams.put("sourceNode", thisNodeNum + "");
        namedParams.put("counter", counter + "");
        JSONRPC2Request request = new JSONRPC2Request("forwardKeyRemove", namedParams, this.nodeNum);
        System.out.println(request + "\n" + successorIPs[2]);
        sendRequest(request, "http://" + successorIPs[2] + SERVER_PORT);
        return;
    }

    /**
     * Find a Key if it exists in the network and return where the key is at.
     * @param value The key we're looking for in the network
     * @param sourceNode The node where this request originated from
     * @param found If the node has been found.
     * @param keyNode The node where the key is at.
     */
    public void findKey(String value, int sourceNode, Boolean found, int keyNode) {
        String input;
        if (value == null) {
            // Get user input for what String is needed.
            System.out.println("Please enter the key value to find:");
            System.out.print(">> ");
            Scanner scan = new Scanner(System.in);
            input = scan.nextLine();
            System.out.println("");
        } else {
            input = value;
        }
        // Hash key to figure out what node to send it too
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (md == null) {
            System.err.println("Something wrong with Message Digest");
            return;
        }
        md.update(input.getBytes());

        byte[] byteData = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        // Get node information to send too.
        BigInteger hashedBigInt = new BigInteger(byteData);
        Integer i = this.totalNodes;
        byte zero = 0;
        byte[] ba = new byte[]{zero, i.byteValue()};
        BigInteger nodeForKey = hashedBigInt.mod(new BigInteger(ba));
        if (!this.keys.isEmpty()) {
            for (String s : this.keys) {
                if (s.equalsIgnoreCase(input)) {
                    found = true;
                    keyNode = this.nodeNum;
                }
            }
        }
        // NODE NOT AT LOCAL NODE...

        if (sourceNode == this.nodeNum) {
            if (found) {
                System.out.println("Key Found at Node : " + keyNode);
            } else {
                System.out.println("Key NOT found in the network");
            }
            return;
        }
        if (sourceNode == -1) {
            sourceNode = this.nodeNum;
        }
        // FINGERTABLE VARIABLE SINCE WERE ALWAYS GETTING IT
        // If client node is a direct successor from Fingertable
        // Get variables needed for checking if 
        List<String> nodeKeys = this.keys;
        String successorIP = this.fingerTable.getSuccessorsIPs()[0];

        if (!nodeKeys.isEmpty()) {
            for (String s : nodeKeys) {
                if (s.equalsIgnoreCase(input)) {
                    found = true;
                    keyNode = this.nodeNum;
                }
            }
        }

        HashMap<String, Object> namedParams = new HashMap<>();
        namedParams.put("key", input);
        namedParams.put("sourceNode", sourceNode + "");
        namedParams.put("found", found + "");
        namedParams.put("keyNode", keyNode + "");
        JSONRPC2Request request = new JSONRPC2Request("findKey", namedParams, this.nodeNum);
        sendRequest(request, "http://" + successorIP + SERVER_PORT);
        return;
    }
    
    /**
     * Debug method to print all the current online nodes within the network.
     */
    public void printOnlineNodes() {
        String method = "printAllNodes";
        int requestID = this.nodeNum;
        JSONRPC2Request request = new JSONRPC2Request(method, requestID);
        boolean succ = sendRequest(request, getServerIP());
        if (succ) {
            System.out.println(this.latestResponse.getResult());
        } else {
            System.out.println("Unsuccessful printOnlineNodes");
        }
    }

    /**
     * Closes Program
     */
    public void exit() {
        closeClient();
        System.out.println("Shutting Down...");
        System.exit(0);
    }

    /**
     * Prints the clients options
     */
    public void printOptions() {
        System.out.println("\n");
        System.out.println("/***********************************************\\");
        System.out.println("|  Options listed below.  Type and press enter  |");
        System.out.println("|-----------------------------------------------|");
        System.out.println("| 1. Print Fingertable  |  2. Prnt onlne nodes  |");
        System.out.println("| 3. Submit key         |  4. List held keys    |");
        System.out.println("| 5. Remove key         |  6. Find key          |");
        System.out.println("| 7. Print node info    |  8. Exit program      |");
        System.out.println("\\***********************************************/");
        System.out.print(">> ");
    }

    /**
     * Adds a given key to this clients list of keys
     * @param key They key To add
     * @return whether the key add was successful. Will return false if key already exists
     */
    public boolean addKeyToList(String key) {
        if (this.keys.contains(key)) {
            return false;
        } else {
            return keys.add(key);
        }
    }

    public boolean removeKeyFromList(String key) {
        return keys.remove(key);
    }

    public boolean isKeyInList(String key) {
        return keys.contains(key);
    }

    /**
     * Main method
     */
    public static void main(String[] args) {
        try {
            System.out.println("Client starting up...");
            Scanner scan = new Scanner(System.in);
            System.out.println("What degree is this Chord ring? 2^x");
            int degree = Integer.parseInt(scan.nextLine());
            System.out.println("What node number are you?");
            int nodeNum = Integer.parseInt(scan.nextLine());
            System.out.println("What's the IP of the registration server?");
            String serverIP = "http://" + scan.nextLine() + SERVER_PORT;
            Client client = new Client(nodeNum, degree, serverIP);
            scan = null;
            client.start();
            new SuccessorUpdaterThread(client).start();
            new RequestHandlerThread(client.recieve, client).start();
            new InitialKeyRetreval(client).start();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Get's all the keys that belong to the given client
     * @param hashValue The value of keys to hash to and return
     * @return The Map of the keys
     */
    public Map getClientsKeys(int hashValue) {
        Map toReturn = new HashMap<String, Object>();
        List<String> keys = this.keys;
        int counter = 0;
        if (keys.isEmpty()) {
            toReturn.put("numParams", -1 + "");
        } else {
            for (String s : keys) {
                // START HASHING
                MessageDigest md = null;
                try {
                    md = MessageDigest.getInstance("SHA-256");
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (md == null) {
                    System.err.println("Something wrong with Message Digest");
                    return null;
                }
                md.update(s.getBytes());

                byte[] byteData = md.digest();

                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < byteData.length; i++) {
                    sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
                }
                // Get node information to send too proper node.
                BigInteger hashedBigInt = new BigInteger(byteData);
                Integer i = this.totalNodes;
                byte zero = 0;
                byte[] ba = new byte[]{zero, i.byteValue()};
                BigInteger nodeForKey = hashedBigInt.mod(new BigInteger(ba));
                int result = Integer.parseInt((String) nodeForKey.toString());
                // FINISH HASHING
                if (result == hashValue) {
                    counter++;
                    toReturn.put("key" + counter, s);
                }
            }
            toReturn.put("numParams", counter + "");
            if (counter > 0) {
                for (int i = 1; i <= counter; i++) {
                    String s = (String) toReturn.get("key" + i);
                    this.removeKeyFromList(s);
                }
            }
        }
        return toReturn;
    }

    /**
     * This method handles the communication with the successornode to retrieve the keys that belong to this node.
     */
    public void initialKeyRetreval() {
        String successorIP = this.getFingerTable().getSuccessorsIPs()[0];
        Map params = new HashMap<String, Object>();
        JSONRPC2Request request = new JSONRPC2Request("checkForNodeKeys", params, this.nodeNum);
        JSONRPC2Response response = sendRequestWithResponse(request, "http://" + successorIP + this.SERVER_PORT);
        Map requestParams = (HashMap) response.getResult();
        if (requestParams == null) {
            return;
        }
        int numParams = Integer.parseInt((String) requestParams.get("numParams"));
        if (numParams == -1) {
            System.out.println("No keys from successor.");
        } else {
            int counter = numParams;
            String[] newKeys = new String[counter];
            for (int i = 0; i < newKeys.length; i++) {
                newKeys[i] = (String) requestParams.get("key" + (i + 1));
            }
            for (String s : newKeys) {
                this.addKeyToList(s);
            }
        }

        return;
    }

    /**
     * This thread handles the Successor updates
     */
    public static class SuccessorUpdaterThread extends Thread {

        private Client client;
        private static final int SUCCESSOR_THREAD_SLEEP_TIME = 3000;

        public SuccessorUpdaterThread(Client client) {
            this.client = client;
        }

        @Override
        public void run() {
            while (true) { // run method getCurrentClientIPs
                URL serverURL = null;
                try {
                    serverURL = new URL(client.getServerIP());
                } catch (MalformedURLException e) {
                    System.err.println("Something went wrong constructing ServerURL in successor thread");
                    System.err.println(e.getMessage());
                }

                HashMap<String, Object> params = new HashMap<>();
                params.put("numParams", client.n);
                for (int i = 0; i < client.n; i++) {
                    params.put("param" + i, client.getFingerTable().getK()[i] + "");
                }
                params.put("predecessor", this.client.predecessorNum);
                JSONRPC2Request request = new JSONRPC2Request("getCurrentClientIPs", params, this.client.getClientNodeNum());
                JSONRPC2Session mySession = new JSONRPC2Session(serverURL);

                JSONRPC2Response response = null;

                try {
                    response = mySession.send(request);
                } catch (JSONRPC2SessionException e) {
                    System.err.println("Something went wrong sending request/getting response in successor thread");
                    System.err.println(e.getMessage());
                }
                Map responseParams = (HashMap) response.getResult();

                this.client.setLatestSuccessorResponse(response);
                // Do logic for updating Clients successor field in FT

                // Print response result / error
                if (response.indicatesSuccess()) {
                    int[] successors = new int[client.n];
                    String[] ips = new String[client.n];
                    for (int i = 0; i < client.n; i++) {
                        int CNparam = Integer.parseInt((String) responseParams.get("CNparam" + i));
                        String IPparam = (String) responseParams.get("IPparam" + i);
                        successors[i] = CNparam;
                        ips[i] = IPparam;
                    }
                    this.client.predecessorActual = Integer.parseInt((String) responseParams.get("actualPred") + "");
                    this.client.predecessorIP = (String) responseParams.get("actualPredIP");
                    // UPDATE CLIENT SUCCESSOR LIST HERE.
                    client.updateSuccessors(successors, ips);
                } else {
                    System.err.println(response.getError().getMessage());
                }

                try {
                    Thread.sleep(SUCCESSOR_THREAD_SLEEP_TIME);
                } catch (InterruptedException ex) {
                    System.err.println("ERROR: SUCCESSORUPDATETHREAD SLEEP TIMER");
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Thread to handle the Innerclass thread.
     */
    public static class RequestHandlerThread extends Thread {

        ServerSocket socket;
        Client client;

        public RequestHandlerThread(ServerSocket s, Client c) {
            this.socket = s;
            this.client = c;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    new RequestHandler(socket.accept(), client).start();
                } catch (IOException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    /**
     * Inner class to handle the requests
     */
    public static class RequestHandler extends Thread {

        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;
        private Dispatcher dispatcher;
        private Client client;

        /**
         * Constructs a handler thread, squirreling away the socket. All the
         * interesting work is done in the run method.
         */
        public RequestHandler(Socket socket, Client client) {
            this.socket = socket;
            this.client = client;
            // Create a new JSON-RPC 2.0 request dispatcher
            this.dispatcher = new Dispatcher();

            dispatcher.register(new JsonHandlerClient.ClientHandler(getClient()));

        }

        public Client getClient() {
            return this.client;
        }

        /**
         * Services this thread's client by repeatedly requesting a screen name
         * until a unique one has been submitted, then acknowledges the name and
         * registers the output stream for the client in a global set, then
         * repeatedly gets inputs and broadcasts them.
         */
        public void run() {
            try {
                // Create character streams for the socket.
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

                // read request
                String line;
                line = in.readLine();
                StringBuilder raw = new StringBuilder();
                raw.append("" + line);
                boolean isPost = line.startsWith("POST");
                int contentLength = 0;
                while (!(line = in.readLine()).equals("")) {
                    raw.append('\n' + line);
                    if (isPost) {
                        final String contentHeader = "Content-Length: ";
                        if (line.startsWith(contentHeader)) {
                            contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                        }
                    }
                }
                StringBuilder body = new StringBuilder();
                if (isPost) {
                    int c = 0;
                    for (int i = 0; i < contentLength; i++) {
                        c = in.read();
                        body.append((char) c);
                    }
                }
                JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
                JSONRPC2Response resp = dispatcher.process(request, null);

                // send response
                out.write("HTTP/1.1 200 OK\r\n");
                out.write("Content-Type: application/json\r\n");
                out.write("\r\n");
                out.write(resp.toJSONString());
                // do not in.close();
                out.flush();
                out.close();
            } catch (IOException e) {
                System.out.println(e);
            } catch (JSONRPC2ParseException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    System.err.println(e);
                }
            }
        }
    }

    /**
     * Inner thread class to handle the initial key retreval when a node joins the network
     */
    public static class InitialKeyRetreval extends Thread {

        Client client;

        public InitialKeyRetreval(Client c) {
            this.client = c;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
            client.initialKeyRetreval();
            return;
        }
    }
}
