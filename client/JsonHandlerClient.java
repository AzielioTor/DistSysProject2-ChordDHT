package client;

/**
 * Demonstration of the JSON-RPC 2.0 Server framework usage. The request
 * handlers are implemented as static nested classes for convenience, but in
 * real life applications may be defined as regular classes within their old
 * source files.
 *
 * @author Vladimir Dzhuvinov
 * @version 2011-03-05
 */
import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;
import java.util.HashMap;
import java.util.Map;

public class JsonHandlerClient {

    // Implements a handler for All Servers
    // that return the current date and time
    public static class ClientHandler implements RequestHandler {

        private Client client;

        public ClientHandler(Client client) {
            this.client = client;
        }

        // Reports the method names of the handled requests
        public String[] handledRequests() {

            return new String[]{"findKey", "triggerSuccessorUpdate", "addKey",
                "removeKey", "forwardKeyAdd", "forwardKeyRemove",
                "checkForNodeKeys"};
        }

        // Processes the requests
        public JSONRPC2Response process(JSONRPC2Request req, MessageContext ctx) {
            // This statement registers nodes onto the server
            if (req.getMethod().equals("triggerSuccessorUpdate")) {
//                Map hm = req.getNamedParams();
//                String clientIP = (String) hm.get("ClientIP");
//                int nodeNum = Integer.parseInt((String) hm.get("ClientNodeNum"));
//                boolean success = rs.addClient(nodeNum, true, clientIP);
//                String responseString = "Client added :   " + success;
//                return new JSONRPC2Response(responseString, req.getID());

            } else if (req.getMethod().equals("findKey")) {
                Map hm = req.getNamedParams();
                String responseString = "";
                String key = (String) hm.get("key");
                int sourceNode = Integer.parseInt((String) hm.get("sourceNode"));
                boolean found = Boolean.parseBoolean((String) hm.get("found"));
                int keyNode = Integer.parseInt((String) hm.get("keyNode"));
                client.findKey(key, sourceNode, found, keyNode);
                return new JSONRPC2Response(responseString, req.getID());
            } else if (req.getMethod().equals("addKey")) {
                Map hm = req.getNamedParams();
                String responseString = "";
                String key = (String) hm.get("key");
                boolean success = client.addKeyToList(key);
                if (success) {
                    responseString = "Key added successfully";
                } else {
                    responseString = "Key added unsuccessfully";
                }
                return new JSONRPC2Response(responseString, req.getID());
            } else if (req.getMethod().equals("removeKey")) {
                Map hm = req.getNamedParams();
                String responseString = "";
                String key = (String) hm.get("key");
                boolean success = client.removeKeyFromList(key);
                if (success) {
                    responseString = "Key removed successfully";
                } else {
                    responseString = "Key removed unsuccessfully";
                }
                return new JSONRPC2Response(responseString, req.getID());

                //// FORWARDING METHODS
            } else if (req.getMethod().equals("forwardKeyAdd")) {
                Map hm = req.getNamedParams();
                String responseString = "";
                String key = (String) hm.get("key");
                int sourceNode = Integer.parseInt((String) hm.get("sourceNode"));
                int counter = Integer.parseInt((String) hm.get("counter"));
                System.out.println("Forwarding key : " + key + "...");
                client.submitKey(key, sourceNode, counter);
                responseString = "Key forwarded";
                System.out.println("Forwarding Key");
                client.printOptions();
                return new JSONRPC2Response(responseString, req.getID());
            } else if (req.getMethod().equals("forwardKeyRemove")) {
                Map hm = req.getNamedParams();
                String responseString = "";
                String key = (String) hm.get("key");
                int sourceNode = Integer.parseInt((String) hm.get("sourceNode"));
                int counter = Integer.parseInt((String) hm.get("counter"));
                System.out.println("Removing key : " + key + "...");
                client.removeKey(key, sourceNode, counter);
                responseString = "Key forwarded";
                System.out.println("Forwarding Key");
                client.printOptions();
                return new JSONRPC2Response(responseString, req.getID());
            } else if (req.getMethod().equals("checkForNodeKeys")) {
                int hashValue = Integer.parseInt(Long.toString((long) req.getID()));
                Map response = client.getClientsKeys(hashValue);
                return new JSONRPC2Response(response, req.getID());
            } else {
                return new JSONRPC2Response(JSONRPC2Error.METHOD_NOT_FOUND, req.getID());
            }
            return new JSONRPC2Response(JSONRPC2Error.INTERNAL_ERROR, req.getID());
        }
    }
}
