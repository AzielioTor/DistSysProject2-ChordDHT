/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import client.Client;
import java.io.File;

/**
 *
 * @author Azielio
 */
public interface ServerNode {

    public void printActiveNodes();

    public boolean acceptNodeConnection();

    public boolean registerNode();

    public boolean acceotNodeDisconnection();

    public boolean unregisterNode();

    public boolean addNode(Client client);

    public boolean removeNode();

    public File getFile(String filename);

    public boolean submitFile(String filename);

}
