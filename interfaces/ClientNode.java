/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import fingertable.FingerTable;

/**
 *
 * @author Azielio
 */
public interface ClientNode {

    public ClientNode getSuccessor();

    public FingerTable getFingerTable();

    public void printFingerTable();

    public boolean joinChordNetwork(); // Make private

    public boolean leaveChordNetwork(); // Make private

}
