/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chorddht;

import client.Client;
import fingertable.FingerTable;
import java.util.Scanner;
import server.RegistrationServer;

/**
 *
 * @author AzielioTor - 02/25/2018
 */
public class ChordDHT {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
//        testFingerTable();
        Scanner scan = new Scanner(System.in);
        String inp = scan.nextLine();
        if (inp.equalsIgnoreCase("c")) {
            System.out.println("Client");
            Client.main(args);
        }
        if (inp.equalsIgnoreCase("s")) {
            System.out.println("Server");
            RegistrationServer.main(args);
        }
    }

    public static void testFingerTable() {
        FingerTable ft1 = new FingerTable(null, 1, 4);
        FingerTable ft2 = new FingerTable(null, 2, 4);
        FingerTable ft8 = new FingerTable(null, 8, 4);
        FingerTable ft14 = new FingerTable(null, 14, 4);
        System.out.println(ft1 + "\n");
        System.out.println(ft2 + "\n");
        System.out.println(ft8 + "\n");
        System.out.println(ft14 + "\n");

    }

}
