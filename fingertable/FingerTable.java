/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fingertable;

import client.Client;

/**
 * This method handles all the FingerTable logic for the client nodes.
 * @author AzielioTor
 */
public class FingerTable {

    private Client owner;
    private int nodeNum;
    private int n;
    private int[] k;
    private int[] successor;
    private String[] successorIPs; // NOTE: IP addresses take at most 15 chars

    public FingerTable(Client owner, int nodeNum, int n) {
        this.owner = owner;
        this.nodeNum = nodeNum;
        this.n = n;
        this.successor = new int[this.n];
        this.successorIPs = new String[this.n];
        initTable();
    }

    private void initTable() {
        k = new int[n];
        for (int index = 0; index < this.n; index++) {
            k[index] = (this.nodeNum + ((int) Math.pow(2, index))) % ((int) Math.pow(2, this.n));
        }
        for (int index = 0; index < this.n; index++) {
            if (owner != null) {
                successorIPs[index] = owner.getClientIP();
            } else {
                successorIPs[index] = "";
            }
        }
    }

    public int[] getSuccessors() {
        return this.successor;
    }

    public void updateSuccessors(int[] successors) {
        this.successor = successors;
    }

    public String[] getSuccessorsIPs() {
        return this.successorIPs;
    }

    public void updateSuccessorsIPs(String[] ips) {
        this.successorIPs = ips;
    }

    public int[] getK() {
        return this.k;
    }

    /**
     * Returns the String representation of the FingerTable of a node. NOTE:
     * Only formatted for a Chord ring of less than 100
     */
    @Override
    public String toString() {
        // TODO: Add space for IP addresses '*****************'

        String tR = "\n/***********|***********|***********|***********|*****************\\\n";
        tR = tR.concat("|     i     |   k+2^i   | Pointing  | Successor |   IP Address    |\n");
        tR = tR.concat("|-----------|-----------|-----------|-----------|-----------------|\n");
        for (int index = 0; index < this.n; index++) {
            int i = index;
            int k = this.k[index];
            String pointing = "1/" + (int) (Math.pow(2, (n - i)));
            int successorNode = successor[index];
            String box1, box2, box3, box4;

            if (i > 9) {
                box1 = "     " + i + "     ";
            } else {
                box1 = "     " + i + "     ";
            }

            if (k > 9) {
                box2 = "     " + k + "    ";
            } else {
                box2 = "     " + k + "     ";
            }

            if (pointing.length() > 3) {
                box3 = "    " + pointing + "   ";
            } else {
                box3 = "    " + pointing + "    ";
            }

            if (successorNode > 9 || successorNode < 0) {
                box4 = "     " + successorNode + "    ";
            } else {
                box4 = "     " + successorNode + "     ";
            }

            // 17chars long
            String ip_address = " ";
            String currIP = this.successorIPs[index];
            int numSpaces = 15 - currIP.length();
            ip_address += currIP;
            String spaces = "";
            for (int j = 0; j < numSpaces; j++) {
                spaces += " ";
            }
            ip_address += spaces + " ";

            tR = tR.concat("|" + box1 + "|" + box2 + "|" + box3 + "|" + box4 + "|" + ip_address + "|\n");
        }
        tR = tR.concat("\\***********|***********|***********|***********|*****************/\n");
        return tR;
    }

}
